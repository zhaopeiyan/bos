package cn.itcast.bos.utils;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import cn.itcast.bos.domain.User;

/**
 * <p>Title:BosUtils</p>
 * <p>Description:BOS系统工具类</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年4月13日 下午7:43:59
 */
public class BosUtils {
	
	/**
	 * 获取当前系统的session对象
	 * @return
	 */
	public static HttpSession getSession(){
		return ServletActionContext.getRequest().getSession();
	}

	/**
	 * 获取session中的登陆用户的信息
	 * @return
	 */
	public static User getUser() {
		// TODO Auto-generated method stub
		return (User) getSession().getAttribute("loginUser");
	}

}
