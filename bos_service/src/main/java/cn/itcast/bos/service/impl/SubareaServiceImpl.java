package cn.itcast.bos.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.ISubareaDao;
import cn.itcast.bos.domain.Subarea;
import cn.itcast.bos.service.ISubareaService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class SubareaServiceImpl implements ISubareaService {

	@Resource
	private ISubareaDao subareaDao;

	@Override
	public void add(Subarea model) {
		subareaDao.save(model);
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		subareaDao.pageQuery(pageBean);
	}

	@Override
	public List<Subarea> findAll() {
		return subareaDao.findAll();
	}

	@Override
	public List<Subarea> findNoLinked() {
		return subareaDao.findByConditions("staff.findNoLinked");
	}

	@Override
	public void batchSave(List<Subarea> list) {
		for(Subarea subarea : list ){
			subareaDao.saveOrUpdate(subarea);
		}
	}

	@Override
	public List<Subarea> findSubareasByDecidedzone(String decidedzoneId) {
		
		return subareaDao.findByConditions("subarea.findSubareaByDecidedzone", decidedzoneId);
	}

	@Override
	public void delete(String ids) {
		String[] split = ids.split(",");
		for(String id : split){
			subareaDao.executeUpdate("subarea.delete", id);
		}
	}

	@Override
	public List<?> findGroupedSubareas() {
		return subareaDao.findGroupedSubareas();
	}

	@Override
	public void edit(Subarea model) {
		subareaDao.saveOrUpdate(model);
	}

	
	
}
