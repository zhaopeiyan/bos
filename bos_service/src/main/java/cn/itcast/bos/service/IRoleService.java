package cn.itcast.bos.service;

import java.util.List;

import cn.itcast.bos.domain.Role;
import cn.itcast.bos.utils.PageBean;

public interface IRoleService {

	void add(Role model, String ids);

	void pageQuery(PageBean pageBean);

	List<Role> findAll();

	Role findRoleById(String id);

	void edit(Role model, String ids);

	void delete(String ids);

}
