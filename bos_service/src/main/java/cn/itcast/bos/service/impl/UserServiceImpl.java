package cn.itcast.bos.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IUserDao;
import cn.itcast.bos.domain.Role;
import cn.itcast.bos.domain.User;
import cn.itcast.bos.service.IUserService;
import cn.itcast.bos.utils.BosUtils;
import cn.itcast.bos.utils.MD5Utils;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class UserServiceImpl implements IUserService<User> {

	@Autowired
	private IUserDao userDao;
	
	@Override
	public User login(String username, String md5) {
		return userDao.login(username,md5);
	}

	@Override
	public void editPwd(User model) {
		//1.从session中获取用户
		User loginUser = BosUtils.getUser();
		model.setPassword(MD5Utils.md5(model.getPassword()));
//		userDao.executeUpdate("user.editPwd",model.getPassword(),loginUser.getId());
		userDao.executeUpdate("user.editPwd", model.getPassword(),loginUser.getId());
	}

	@Override
	public User findByUsername(String username) {
		User user = userDao.findByUsername(username);
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void add(User model,String[] ids) {
		//1.保存用户数据
		userDao.save(model);
		//2.保存用户关联角色表
		if(ids.length > 0 ){
			for(String id : ids){
				Role role = new Role();
				role.setId(id);
				model.getRoles().add(role);
			}
		} 
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		userDao.pageQuery(pageBean);
	}
	
	

}
