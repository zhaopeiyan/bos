package cn.itcast.bos.service.impl;

import java.sql.Timestamp;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IDecidedzoneDao;
import cn.itcast.bos.dao.INoticebillDao;
import cn.itcast.bos.dao.IWorkbillDao;
import cn.itcast.bos.domain.Decidedzone;
import cn.itcast.bos.domain.Noticebill;
import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.domain.User;
import cn.itcast.bos.domain.Workbill;
import cn.itcast.bos.service.INoticebillService;
import cn.itcast.bos.utils.BosUtils;
import cn.itcast.bos.utils.PageBean;
import cn.itcast.crm.service.ICustomerService;

@Service
@Transactional
public class NoticebillServiceImpl implements INoticebillService {

	@Resource
	private INoticebillDao noticebillDao;
	@Resource
	private ICustomerService customerService;
	@Resource
	private IDecidedzoneDao decidedzoneDao;
	@Resource
	private IWorkbillDao workbillDao;

	@Override
	public void add(Noticebill model) {
		//1.保存业务通知单
		noticebillDao.save(model);
		//2.关联业务受理员(当前用户登录)
		User loginUser = BosUtils.getUser();
		if(loginUser != null){
			model.setUser(loginUser);
		}
		//3.尝试自动分单(根据客户的地址,查询客户所属的定区)
		String decidedzone_id = customerService.findDecidedzoneIdByAddress(model.getPickaddress());
		
		//4.查到定区,自动分单
		if(StringUtils.isNotBlank(decidedzone_id)){
			//4.1根据定区id,查询定区对象
			Decidedzone decidedzone = decidedzoneDao.findById(decidedzone_id);
			//4.2从定区对象中获取取派员
			Staff staff = decidedzone.getStaff();
			//4.3关联业务通知单和取派员
			model.setStaff(staff);
			//4.4设置业务通知单的分单类型
			model.setOrdertype("自动分单");
			//4.5给取派员生成工单
			Workbill workbill = new Workbill();
			workbill.setAttachbilltimes(0);
			workbill.setBuildtime(new Timestamp(System.currentTimeMillis()));
			workbill.setNoticebill(model);
			workbill.setPickstate("未取件");
			workbill.setRemark(model.getRemark());
			workbill.setStaff(staff);
			workbill.setType("新单");
			workbillDao.save(workbill);
		}else{
			//5.查不到定区,手工分单
			model.setOrdertype("手动分单");
		}
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		noticebillDao.pageQuery(pageBean);
	}

	@Override
	public void update(String staffId, String id) {
		noticebillDao.executeUpdate("noticebill.update", staffId,id);
		noticebillDao.executeUpdate("workbill.update", staffId,id);
	}
	
}
