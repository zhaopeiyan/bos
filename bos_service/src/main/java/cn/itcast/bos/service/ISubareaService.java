package cn.itcast.bos.service;

import java.util.List;

import cn.itcast.bos.domain.Subarea;
import cn.itcast.bos.utils.PageBean;

public interface ISubareaService {

	void add(Subarea model);

	void pageQuery(PageBean pageBean);

	List<Subarea> findAll();

	List<Subarea> findNoLinked();

	void batchSave(List<Subarea> list);

	List<Subarea> findSubareasByDecidedzone(String decidedzoneId);

	void delete(String ids);

	List<?> findGroupedSubareas();

	void edit(Subarea model);



}
