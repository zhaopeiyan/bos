package cn.itcast.bos.service;

import java.util.List;

import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.utils.PageBean;

public interface IStaffService {

	void add(Staff model);

	void pageQuery(PageBean pb);

	void delStaff(String ids);

	void edit(Staff model);

	void reStaff(String ids);

	List<Staff> findNoDel();

}
