package cn.itcast.bos.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IFunctionDao;
import cn.itcast.bos.domain.Function;
import cn.itcast.bos.domain.User;
import cn.itcast.bos.service.IFunctionService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class FunctionServiceImpl implements IFunctionService {
	
	@Resource
	private IFunctionDao functionDao;

	@Override
	public List<Function> findAll() {
		List<Function> list = functionDao.findAll();
		return list;
	}

	@Override
	public void add(Function model) {
		functionDao.save(model);
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		functionDao.pageQuery(pageBean);
	}

	@Override
	public List<Function> findFunctionsByUser(User user) {
		//1.获取到当前登录的用户名
		String username = user.getUsername();
		List<Function> list = null;
		if("admin".equals(username)){
			//2.判断用户名,如果是admin,查询所有权限
			list = functionDao.findAll();
		}else{
			//3.如果是其他用户,根据用户的id查询该用户的权限
			list = functionDao.findByConditions("function.findByUserId", user.getId());
		}
		return list;
	}

	
	/**
	 * 查询所有菜单
	 */
	@Override
	public List<Function> findAllMenu(String id) {
		return functionDao.findByConditions("function.findAllMenu");
	}

	
	/**
	 * 查询部分菜单
	 */
	@Override
	public List<Function> findMenuByUserId(String id) {
		return functionDao.findByConditions("function.findMenuByUserId", id);
	}
}










