package cn.itcast.bos.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IRegionDao;
import cn.itcast.bos.domain.Region;
import cn.itcast.bos.service.IRegionService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class RegionServiceImpl implements IRegionService {

	@Autowired
	private IRegionDao regionDao;


	@Override
	public void batchSave(List<Region> list) {
		for(Region region : list){
			regionDao.saveOrUpdate(region);
		}
	}

	@Override
	public void pageQuery(PageBean pb) {
		regionDao.pageQuery(pb);
	}

	@Override
	public List<Region> findByQ(String q) {
		return regionDao.findByQ(q);
	}

	@Override
	public List<Region> findAll() {
		return regionDao.findAll();
	}

	@Override
	public void add(Region model) {
		regionDao.save(model);
	}

	@Override
	public Region findById(String region_id) {
		return regionDao.findById(region_id);
	}

	@Override
	public void edit(Region model) {
		regionDao.saveOrUpdate(model);
	}

	@Override
	public void delete(String ids) {
		String[] split = ids.split(",");
		for(String id : split){
			regionDao.executeUpdate("region.delete",id);
		}
	}
}
