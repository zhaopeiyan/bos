package cn.itcast.bos.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IDecidedzoneDao;
import cn.itcast.bos.dao.ISubareaDao;
import cn.itcast.bos.domain.Decidedzone;
import cn.itcast.bos.domain.Subarea;
import cn.itcast.bos.service.IDecidedzoneService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class DecidedzoneServiceImpl implements IDecidedzoneService {

	@Resource
	private IDecidedzoneDao decidedzoneDao;
	@Resource
	private ISubareaDao subareaDao;

	@Override
	public void add(Decidedzone model, String[] subareaId) {
		// 1.保存model
		decidedzoneDao.save(model);
		//2.关联定区和分区
		for(String id : subareaId){
			Subarea subarea = subareaDao.findById(id);//持久化对象
			subarea.setDecidedzone(model);
		}
	}

	@Override
	public Decidedzone findById(String decidedzone_id) {
		// TODO Auto-generated method stub
		return decidedzoneDao.findById(decidedzone_id);
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		// TODO Auto-generated method stub
		decidedzoneDao.pageQuery(pageBean);
	}
	
	
	
}
