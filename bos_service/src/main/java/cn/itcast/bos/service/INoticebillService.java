package cn.itcast.bos.service;

import cn.itcast.bos.domain.Noticebill;
import cn.itcast.bos.utils.PageBean;

public interface INoticebillService {

	void add(Noticebill model);

	void pageQuery(PageBean pageBean);

	void update(String staffId, String id);

}
