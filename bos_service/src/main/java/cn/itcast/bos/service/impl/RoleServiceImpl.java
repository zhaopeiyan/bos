package cn.itcast.bos.service.impl;

import java.util.HashSet;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IRoleDao;
import cn.itcast.bos.domain.Function;
import cn.itcast.bos.domain.Role;
import cn.itcast.bos.service.IRoleService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class RoleServiceImpl implements IRoleService {
	
	@Resource
	private IRoleDao roleDao;
	
	@Override
	public void add(Role model ,String ids) {
		//1.保存角色数据
		roleDao.save(model);
		//2.关联权限数据role_function
		if(StringUtils.isNotBlank(ids)){
			String[] split = ids.split(",");
			for(String id : split){
				Function function = new Function();
				function.setId(id);
				model.getFunctions().add(function);
			}
		}
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		roleDao.pageQuery(pageBean);
	}

	@Override
	public List<Role> findAll() {
		return roleDao.findAll();
	}

	@Override
	public Role findRoleById(String id) {
		Role role = roleDao.findById(id);
		return role;
	}

	@Override
	public void edit(Role model, String ids) {
		Role role1 = roleDao.findById(model.getId());
		role1.setCode(model.getCode());
		role1.setDescription(model.getDescription());
		role1.setFunctions(new HashSet<Function>(0));
		if(StringUtils.isNotBlank(ids)){
			String[] split = ids.split(",");
			for(String id : split){
				Function function = new Function();
				function.setId(id);
				role1.getFunctions().add(function);
			}
		}
	}

	@Override
	public void delete(String ids) {
		String[] split = ids.split(",");
		for(String id : split){
			roleDao.executeUpdate("role.delete", id);
		}
	
	}
}
