package cn.itcast.bos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IStaffDao;
import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.service.IStaffService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class StaffServiceImpl implements IStaffService {

	@Autowired
	private IStaffDao staffDao;
	
	@Override
	public void add(Staff model) {
		staffDao.save(model);
	}

	@Override
	public void pageQuery(PageBean pb) {
		staffDao.pageQuery(pb);
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delStaff(String ids) {
		String[] split = ids.split(",");
		for(String id : split){
			staffDao.executeUpdate("staff.delete",id);
		}
	}

	/**
	 * 批量恢复staff
	 */
	@Override
	public void reStaff(String ids) {
		// TODO Auto-generated method stub
		String[] split = ids.split(",");
		for(String s : split){
			staffDao.executeUpdate("staff.restore", s);
		}
	}
	
	/**
	 * 修改staff
	 */
	@Override
	public void edit(Staff model) {
		//1.先拿到该id下的那条数据,放在持久态对象里,使用持久态对象的一级缓存和快照的对比进行更新操作
		Staff editStaff = staffDao.findById(model.getId());
		//2.set修改的数据字段
		editStaff.setEmail(model.getEmail());
		editStaff.setHaspda(model.getHaspda());
		editStaff.setName(model.getName());
		editStaff.setStandard(model.getStandard());
		editStaff.setStation(model.getStation());
		editStaff.setTelephone(model.getTelephone());
	}

	@Override
	public List<Staff> findNoDel() {
		return staffDao.findByConditions("staff.findNoDel");
	}


}
