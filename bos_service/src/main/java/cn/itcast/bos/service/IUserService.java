package cn.itcast.bos.service;

import cn.itcast.bos.domain.User;
import cn.itcast.bos.utils.PageBean;

public interface IUserService<User> {

	User login(String username, String md5);

	void editPwd(User model);

	cn.itcast.bos.domain.User findByUsername(String username);

	void add(User model, String[] roleIds);

	void pageQuery(PageBean pageBean);

	
	
}
