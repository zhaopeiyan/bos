package cn.itcast.bos.service;

import cn.itcast.bos.domain.Workordermanage;
import cn.itcast.bos.utils.PageBean;

public interface IWorkordermanageService {

	void pageQuery(PageBean pageBean);

	void add(Workordermanage model);

}
