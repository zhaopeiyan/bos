package cn.itcast.bos.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.IWorkbillDao;
import cn.itcast.bos.domain.Workbill;
import cn.itcast.bos.service.IWorkbillService;
import cn.itcast.bos.utils.PageBean;

@Service
@Transactional
public class WorkbillServiceImpl implements IWorkbillService {

	@Resource
	private IWorkbillDao workbillDao;

	@Override
	public void pageQuery(PageBean pageBean) {
		workbillDao.pageQuery(pageBean);
	}

	@Override
	public List<Workbill> findNewWorkbills() {
		return workbillDao.findByConditions("workbill.findNewWorkbills");
	}
	
}
