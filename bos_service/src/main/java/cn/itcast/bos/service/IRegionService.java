package cn.itcast.bos.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.itcast.bos.domain.Region;
import cn.itcast.bos.utils.PageBean;

public interface IRegionService {

	void batchSave(List<Region> list);

	void pageQuery(PageBean pb);

	List<Region> findByQ(String q);

	List<Region> findAll();

	void add(Region model);

	Region findById(String region_id);

	void edit(Region model);

	void delete(String ids);

}
