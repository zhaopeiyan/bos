package cn.itcast.bos.realm;

import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import cn.itcast.bos.domain.Function;
import cn.itcast.bos.domain.User;
import cn.itcast.bos.service.IFunctionService;
import cn.itcast.bos.service.IUserService;

public class BosLoginRealm extends AuthorizingRealm {

	@Resource
	private IUserService userService;
	@Resource
	private IFunctionService functionService;
	
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalcollection) {
		//1.获取当前登录的用户
		User user = (User) principalcollection.getPrimaryPrincipal();
		//2.从数据库获取权限信息
		List<Function> list = functionService.findFunctionsByUser(user);
		SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
		if(null != list && list.size() > 0){
			for(Function function : list){
				//3.通过authorizationInfo将当前登录用户的权限返回给安全管理器
				simpleAuthorizationInfo.addStringPermission(function.getCode());
			}
		}
		return simpleAuthorizationInfo;
	}

	
	/**
	 * 认证方法
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationtoken)
			throws AuthenticationException {
		UsernamePasswordToken upt = (UsernamePasswordToken) authenticationtoken;
		String username = upt.getUsername();
		//1.根据用户名查询到用户对象
		User user = userService.findByUsername(username);
		if(null != user){
			//2.如果查询到用户对象,通过AuthenticationInfo将数据	返回给安全管理器,安全管理器会校验用户名和密码,正确,正常执行,错误,抛异常
			return  new SimpleAuthenticationInfo(user, user.getPassword(), this.getName());
		}
		//3.查不到,直接返回null
		return null;
	}

}
