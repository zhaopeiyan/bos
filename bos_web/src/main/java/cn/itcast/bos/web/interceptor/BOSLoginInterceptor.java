package cn.itcast.bos.web.interceptor;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

import cn.itcast.bos.domain.User;
import cn.itcast.bos.utils.BosUtils;

/**
 * <p>Title:BOSLoginInterceptor</p>
 * <p>Description:用户登录拦截器</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年4月13日 下午5:22:17
 */
public class BOSLoginInterceptor extends MethodFilterInterceptor {

	/* (non-Javadoc)
	 * @see com.opensymphony.xwork2.interceptor.MethodFilterInterceptor#doIntercept(com.opensymphony.xwork2.ActionInvocation)
	 */
	@Override
	protected String doIntercept(ActionInvocation invocation) throws Exception {
		//获得已登录的用户信息
		User loginUser = BosUtils.getUser();
		//User loginUser = (User) ActionContext.getContext().getSession().get("loginUser");
		if(loginUser == null){
			//未登录,拦截返回login.jsp
			return "login";
		}
		//已登陆,放行
		return invocation.invoke();
	}

}
