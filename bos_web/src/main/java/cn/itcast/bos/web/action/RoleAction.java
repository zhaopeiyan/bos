package cn.itcast.bos.web.action;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Function;
import cn.itcast.bos.domain.Role;
import cn.itcast.bos.service.IRoleService;
import cn.itcast.bos.utils.UUIDUtils;

@Controller
@Scope("prototype")
public class RoleAction extends BaseAction<Role> {

	@Resource
	private IRoleService roleService;
	
	
	private String ids;
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public String listajax(){
		List<Role> list = roleService.findAll();
		String[] exclude = {"functions","users"};
		writeList2Json(list, exclude );
		return NONE;
	}
	
	/**
	 * 分页查询
	 * @return
	 */
	public String pageQuery(){
		roleService.pageQuery(pageBean);
		String[] exclude = {"functions","users"};
		writeObject2Json(pageBean, exclude);
		return NONE;
	}
	
	/**
	 * 添加
	 * @return
	 */
	public String add(){
		model.setId(UUIDUtils.getUUID());
		roleService.add(model,ids);
		return "list";
	}
	
	/**
	 * 更新
	 */
	public String update(){
		roleService.edit(model,ids);
		return "list";
	}
	
	/**
	 * 根据id查询role信息
	 */
	private String ids2;
	public String getIds2() {
		return ids2;
	}
	public String findRoleById(){
		Role role = roleService.findRoleById(model.getId());
		ServletActionContext.getServletContext().setAttribute("role", role);
		Set<Function> functions = role.getFunctions();
		ids2 = "";
		for(Function f : functions){
			ids2 += f.getId() + ",";
		}
		return "edit";
	}
	
	
	/**
	 * 删除
	 */
	public String delete(){
		roleService.delete(ids);
		return "list";
	}
	
}
