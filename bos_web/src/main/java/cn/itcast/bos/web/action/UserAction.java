package cn.itcast.bos.web.action;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.User;
import cn.itcast.bos.service.IUserService;
import cn.itcast.bos.utils.BosUtils;
import cn.itcast.bos.utils.MD5Utils;
import cn.itcast.bos.utils.UUIDUtils;

@Controller//相当于<bean id="userAction" class=""></bean>
@Scope("prototype")//配置action为多实例
public class UserAction extends BaseAction<User> {

	@Resource
	private IUserService userService;
	
	private String checkcode;

	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}
	
	public String pageQuery(){
		userService.pageQuery(pageBean);
		String[] exclude = {"noticebills","roles"};
		writeObject2Json(pageBean, exclude );
		return NONE;
	}
	
	
	private String[] roleIds;
	public void setRoleIds(String[] roleIds) {
		this.roleIds = roleIds;
	}

	/**
	 * 添加user
	 * @return
	 */
	public String add(){
		model.setId(UUIDUtils.getUUID());
		model.setPassword(MD5Utils.md5(model.getPassword()));
		userService.add(model,roleIds);
		return "list";
	}
	
	
	/**
	 * 修改密码action
	 * @return
	 */
	public String editPwd(){
		//1.定义标识位flag
		String flag = "1";
		try {
			userService.editPwd(model);
		} catch (Exception e) {
			flag = "0";
			e.printStackTrace();
		}
		//2.将标志位返回到前台
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(flag);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return NONE;
	}
	
	/**
	 * 使用shiro进行验证登录实现
	 * @return
	 */
	public String login(){
		String validateS = (String) ServletActionContext.getRequest().getSession().getAttribute("key");
		if(StringUtils.isNotBlank(checkcode) && checkcode.equals(validateS)){
			try {
				//使用shiro进行认证
				//1.获取subject对象
				Subject subject = SecurityUtils.getSubject();//未认证状态
				UsernamePasswordToken authenticationtoken = new UsernamePasswordToken(model.getUsername(),MD5Utils.md5(model.getPassword()));
				//2.通过subject调用login进行认证
				subject.login(authenticationtoken);
				//不报异常,登陆成功,将当前的用户放到session,跳转到主界面
				//获取当前登录用户
				User user = (User) subject.getPrincipal();
				BosUtils.getSession().setAttribute("loginUser", user);
				return "home";
			} catch (Exception e) {
				e.printStackTrace();
			}
			//报异常,登录失败,提示用户名或密码错误,跳转到登录页面
			this.addActionError("用户名或密码错误!");
			return "login";
		}
		this.addActionError("验证码错误!");
		return "login";
	}
	
	
	/**
	 * 登陆action
	 * @return
	 */
	/*public String login(){
		String validateS = (String) ServletActionContext.getRequest().getSession().getAttribute("key");
		if(StringUtils.isNotBlank(checkcode) && checkcode.equals(validateS)){
			//校验成功
			System.out.println(MD5Utils.md5(model.getPassword()));
			User user = (User) userService.login(model.getUsername(),MD5Utils.md5(model.getPassword()));
			if(null != user){
				ServletActionContext.getRequest().getSession().setAttribute("loginUser", user);
				return "home";
			}else{
				this.addActionError("用户名或密码错误!");
				return "login";
			}
		}else{
			this.addActionError("验证码错误!");
			return "login";
		}
	}*/
	
}
