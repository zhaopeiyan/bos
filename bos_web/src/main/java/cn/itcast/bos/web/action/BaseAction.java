package cn.itcast.bos.web.action;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.bos.domain.Function;
import cn.itcast.bos.utils.PageBean;
import cn.itcast.crm.service.ICustomerService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class BaseAction<T> extends ActionSupport implements ModelDriven<T> {

	protected T model;

	@Override
	public T getModel() {
		return model;
	}

	@Resource
	protected ICustomerService customerService;
	
	public BaseAction() {
		ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
		Type[] types = parameterizedType.getActualTypeArguments();
		Class<T> entityClass = (Class<T>) types[0];
		try {
			model = entityClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}

		DetachedCriteria dc = DetachedCriteria.forClass(entityClass);
		pageBean.setDc(dc);
	}

	protected PageBean pageBean = new PageBean();

	public void setPage(int page) {
		pageBean.setPageNum(page);
	}

	public void setRows(int rows){
		pageBean.setPageSize(rows);
	}

	/**
	 * 将object对象转换成json对象返回
	 * @param obj
	 * @param exclude
	 */
	public void writeObject2Json(Object obj,String[] exclude){
		JsonConfig config = new JsonConfig();
		config.setExcludes(exclude);
		JSONObject jsonObject = JSONObject.fromObject(obj,config);
		ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(jsonObject.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 将集合装换成json对象返回
	 * @param obj
	 * @param exclude
	 */
	public void writeList2Json(Object obj,String[] exclude){
		JsonConfig config = new JsonConfig();
		config.setExcludes(exclude);
		JSONArray jSONArray = JSONArray.fromObject(obj,config);
		ServletActionContext.getResponse().setContentType("text/json;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(jSONArray.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
