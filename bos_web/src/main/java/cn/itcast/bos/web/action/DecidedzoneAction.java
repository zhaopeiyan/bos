package cn.itcast.bos.web.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Decidedzone;
import cn.itcast.bos.service.IDecidedzoneService;
import cn.itcast.crm.service.Customer;
import cn.itcast.crm.service.ICustomerService;

@Controller
@Scope("prototype")
public class DecidedzoneAction extends BaseAction<Decidedzone> {

	@Resource
	private IDecidedzoneService decidedzoneService;
	
	@Resource
	private ICustomerService customerService;
	
	
	private String[] subareaId;
	public void setSubareaId(String[] subareaId) {
		this.subareaId = subareaId;
	}


	private List<Integer> customerIds;
	public void setCustomerIds(List<Integer> customerIds) {
		this.customerIds = customerIds;
	}

	public String assigncustomerstodecidedzone(){
		customerService.assignCustomersToDecidedZone(customerIds, model.getId());
		return "list";
	}
	
	/**
	 * 查询已关联的客户信息
	 */
	public String findAssociationCustomers(){
		List<Customer> list = customerService.findhasassociationCustomers(model.getId());
		String[] exclude = {"workbills","noticebills"};
		this.writeList2Json(list, exclude);
		return NONE;
	}
	
	/**
	 * 查询未关联的客户信息
	 */
	public String findNoAssociationCustomers(){
		List<Customer> list = customerService.findnoassociationCustomers();
		String[] exclude = {"workbills","noticebills"};
		this.writeList2Json(list, exclude);
		return NONE;
	}
	
	
	/**
	 * 分页查询
	 * @return
	 */
	public String pageQuery(){
		decidedzoneService.pageQuery(pageBean);
		
		String[] exclude = {"pageNum", "pageSize", "dc", "decidedzones", "subareas","workbills","noticebills"};
		this.writeObject2Json(pageBean, exclude);
		return NONE;
	}

	public String add(){
		decidedzoneService.add(model,subareaId);
		return "list";
	}
	
}
