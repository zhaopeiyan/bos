package cn.itcast.bos.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Region;
import cn.itcast.bos.service.IRegionService;
import cn.itcast.bos.utils.PageBean;
import cn.itcast.bos.utils.PinYin4jUtils;
import cn.itcast.bos.utils.UUIDUtils;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@Scope("prototype")
public class RegionAction extends BaseAction<Region> {
	
	@Autowired
	private IRegionService regionService;
	
	private File regionFile;
	public void setRegionFile(File regionFile) {
		this.regionFile = regionFile;
	}
	
	private String ids;
	public void setIds(String ids) {
		this.ids = ids;
	}

	public String delete(){
		regionService.delete(ids);
		return "list";
	}
	
	
	public String add(){
		model.setId(UUIDUtils.getUUID());
		regionService.add(model);
		return "list";
	}

	/**
	 * 测试PinYin4jUtils
	 */
	//@Test
	/*public void pinyingTest(){
		String province = "黑龙江省";
		String city = "伊春市";
		String district = "美溪区";
		
		province =  province.substring(0, province.length() - 1);
		city = city.substring(0, city.length() - 1);
		district = district.substring(0, district.length() - 1);
		//拼接字符串
		String temp = province + city + district;
		String[] headByString = PinYin4jUtils.getHeadByString(temp);
		String shortcode = StringUtils.join(headByString,"");
		System.out.println(shortcode);
		
		//citycode:城市码
		String citycode = PinYin4jUtils.hanziToPinyin(city, "");//去空格
		//String citycode = PinYin4jUtils.hanziToPinyin(city);//不去空格
		System.out.println(citycode);
	}*/
	

	public String q;
	public void setQ(String q) {
		this.q = q;
	}

	/**
	 * 修改区域
	 * @return
	 */
	public String edit(){
		regionService.edit(model);
		return "list";
	}
	
	
	
	/**
	 * 查询区域信息
	 * @return
	 */
	public String listajax(){
		List<Region> list = null;
		if(StringUtils.isNotBlank(q)){
			q = q.toUpperCase();
			q = q.replace("", "%");
			System.out.println(q);
			//不为空,根据q查询数据库
			list = regionService.findByQ(q);
		}else{
			//q为空,查询所有
			list = regionService.findAll();
		}
		String[] excludes = {"subareas"};
		this.writeList2Json(list, excludes);
		return NONE;
	}
	
	/**
	 * 分页查询
	 * @return
	 */
	public String pageQuery(){
		
		regionService.pageQuery(pageBean);
		
		String[] exclude = {"pageNum","pageSize","dc","subareas"};
		writeObject2Json(pageBean, exclude);
		return NONE;
	}
	
	
	/**
	 * 导入Excel表
	 * @return
	 */
	public String importXls(){
		//使用POI读取execel文档
		String flag = "1";
		try {
			//1.创建Wordbook对象,读取整个文档
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(regionFile));
			//2.读取某个sheet
			HSSFSheet sheet = wb.getSheetAt(0);
			//3.循环读取某一行
			List<Region> list = new ArrayList<Region>();
			for(Row row : sheet){
				//跳过第一行标题
				int rowNum = row.getRowNum();
				if(0 == rowNum){
					//第一行,跳过
					continue;
				}
				//4.读取一行中的单元格
				String id = row.getCell(0).getStringCellValue();
				String province = row.getCell(1).getStringCellValue();
				String city = row.getCell(2).getStringCellValue();
				String district = row.getCell(3).getStringCellValue();
				String postcode = row.getCell(4).getStringCellValue();
				//5.创建一个region对象,封装数据
				Region region = new Region(id, province, city, district, postcode, null, null, null);
				
				province =  province.substring(0, province.length() - 1);
				city = city.substring(0, city.length() - 1);
				district = district.substring(0, district.length() - 1);
				//拼接字符串
				String temp = province + city + district;
				String[] headByString = PinYin4jUtils.getHeadByString(temp);
				String shortcode = StringUtils.join(headByString,"");
				//System.out.println(shortcode);
				//citycode:城市码
				String citycode = PinYin4jUtils.hanziToPinyin(city, "");//去空格
				//String citycode = PinYin4jUtils.hanziToPinyin(city);//不去空格
				//System.out.println(citycode);
				
				region.setShortcode(shortcode);
				region.setCitycode(citycode);
				
				list.add(region);
			}
			regionService.batchSave(list);
		} catch (Exception e) {
			flag = "0";
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(flag);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return NONE;
	}

}
