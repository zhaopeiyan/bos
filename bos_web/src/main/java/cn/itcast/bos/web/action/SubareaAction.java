package cn.itcast.bos.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Decidedzone;
import cn.itcast.bos.domain.Region;
import cn.itcast.bos.domain.Subarea;
import cn.itcast.bos.service.IDecidedzoneService;
import cn.itcast.bos.service.IRegionService;
import cn.itcast.bos.service.ISubareaService;
import cn.itcast.bos.utils.FileUtils;
import cn.itcast.bos.utils.PageBean;
import cn.itcast.bos.utils.PinYin4jUtils;

@Controller
@Scope("prototype")
public class SubareaAction extends BaseAction<Subarea> {

	@Resource
	private ISubareaService subareaService;
	@Resource
	private IDecidedzoneService decidedzoneService;
	@Resource
	private IRegionService regionService;
	
	private File subareaFile;
	public void setSubareaFile(File subareaFile) {
		this.subareaFile = subareaFile;
	}


	private String decidedzoneId;
	public void setDecidedzoneId(String decidedzoneId) {
		this.decidedzoneId = decidedzoneId;
	}

	public String findSubareasByDecidedzone(){
		List<Subarea> list = subareaService.findSubareasByDecidedzone(decidedzoneId);
		String[] exclude = {"decidedzone","subareas"};
		this.writeList2Json(list, exclude );
		return NONE;
	}

	
	private String ids;
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public String delete(){
		subareaService.delete(ids);
		return "list";
	}
	
	/**
	 * //异步查询区域分区数据并展示
	 * @return
	 */
	public String findGroupedSubareas(){
		List<?> list = subareaService.findGroupedSubareas();
		String[] exclude = {};
		writeList2Json(list, exclude);
		return NONE;
	}
	

	/**
	 * 导入Excel表
	 * @return
	 */
	public String importXls(){
		//使用POI读取execel文档
		String flag = "1";
		try {
			//1.创建Wordbook对象,读取整个文档
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(subareaFile));
			//2.读取某个sheet
			HSSFSheet sheet = wb.getSheetAt(0);
			//3.循环读取某一行
			List<Subarea> list = new ArrayList<Subarea>();
			for(Row row : sheet){
				//跳过第一行标题
				int rowNum = row.getRowNum();
				if(0 == rowNum){
					//第一行,跳过
					continue;
				}
				//4.读取一行中的单元格
				String id = row.getCell(0).getStringCellValue();
				String region_id = row.getCell(1).getStringCellValue();
				String addresskey = row.getCell(2).getStringCellValue();
				String startnum = row.getCell(3).getStringCellValue();
				String endnum = row.getCell(4).getStringCellValue();
				String single = row.getCell(5).getStringCellValue();
				String position = row.getCell(6).getStringCellValue();
				//5.创建一个region对象,封装数据
//				Region region = new Region(id, province, city, district, postcode, null, null, null);
				
				
				Region region = null;
				if(region_id != null){
					region = regionService.findById(region_id);
				}
				
				Subarea subarea = new Subarea(id, null, region, addresskey, startnum, endnum, single, position);
				
				
				list.add(subarea);
			}
			subareaService.batchSave(list);
		} catch (Exception e) {
			flag = "0";
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(flag);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return NONE;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 分区信息的导出
	 * @return
	 */
	public String exportXls(){
		//1.查询所有的分区数据
		List<Subarea> list = subareaService.findAll();
		//2.将list中的数据保存到Excel中
		//2.1创建Excel
		//创建空Excel
		HSSFWorkbook wb= new HSSFWorkbook();
		//在空Excel中创建sheet
		HSSFSheet sheet = wb.createSheet();
		//创建第一行标题
		HSSFRow row = sheet.createRow(0);
		row.createCell(0).setCellValue("分区编号");
		row.createCell(1).setCellValue("区域编号");
		row.createCell(2).setCellValue("关键字");
		row.createCell(3).setCellValue("起始号");
		row.createCell(4).setCellValue("结束号");
		row.createCell(5).setCellValue("单双号");
		row.createCell(6).setCellValue("位置");
		if(null != list && list.size() > 0 ){
			//有数据
			int index = 1;//从第二行开始
			for(Subarea subarea : list){
				//在sheet中循环创建row
				row = sheet.createRow(index++);
				//在row中创建列,添加数据
				row.createCell(0).setCellValue(subarea.getId());
				Region region = subarea.getRegion();
				if(null != region){
					row.createCell(1).setCellValue(region.getId());
				}else{
					row.createCell(1).setCellValue("未定义");
				}
				row.createCell(2).setCellValue(subarea.getAddresskey());
				row.createCell(3).setCellValue(subarea.getStartnum());
				row.createCell(4).setCellValue(subarea.getEndnum());
				row.createCell(5).setCellValue(subarea.getSingle());
				row.createCell(6).setCellValue(subarea.getPosition());
			}
		}
		//3.将Excel通过response返回前台
		String filename = "分区数据.xls";
		//获取到请求头中的用户的浏览器类型User-Agent
		String agent = ServletActionContext.getRequest().getHeader("User-Agent");
		try {
			//3.1一个流两个头
			String mimeType = ServletActionContext.getServletContext().getMimeType(filename);
			filename = FileUtils.encodeDownloadFilename(filename, agent);
			//一个流:指的是response的输出流
			ServletOutputStream os = ServletActionContext.getResponse().getOutputStream();
			ServletActionContext.getResponse().setContentType(mimeType);
			//3.2第二个头  content-disposition  告诉浏览器返回的数据打开的方法
			ServletActionContext.getResponse().setHeader("content-disposition", "attachment;filename="+filename);
			//4.调用workbook的write方法将Excel返回到前台
			wb.write(os);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return NONE;
	}
	
	/**
	 * 添加分区记录
	 * @return
	 */
	public String add(){
		subareaService.add(model);
		return "list";
	}
	
	/**
	 * 根据条件查询,分页
	 * @return
	 * @throws IOException
	 */
	public String pageQuery() throws IOException{
		
		//1.1获取dc
		DetachedCriteria dc = pageBean.getDc();
		//1.2将关键字放入dc中
		String addresskey = model.getAddresskey();
		if(StringUtils.isNotBlank(addresskey)){
			dc.add(Restrictions.like("addresskey", "%"+addresskey+"%"));
		}
		//1.3将省市区添加到dc中
		Region region = model.getRegion();
		if(null != region){
			//在hibernate中如果要进行多表查询,必须对join的表设置别名,如果不设置别名则不能使用
			//如果在一个对象中使用另一个对象,必须给另一个对象起别名
			dc.createAlias("region", "r");
			
			//判断是否输入了省
			String province = region.getProvince();
			if(StringUtils.isNotBlank(province)){
				//说明输入了省
				dc.add(Restrictions.like("r.province", "%"+province+"%"));
			}
			
			//判断是否输入了市
			String city = region.getCity();
			if(StringUtils.isNotBlank(city)){
				dc.add(Restrictions.like("r.city", "%"+city+"%"));
			}
			
			//判断是否输入了区
			String district = region.getDistrict();
			if(StringUtils.isNotBlank(district)){
				dc.add(Restrictions.like("r.district", "%"+district+"%"));
			}
		}
		subareaService.pageQuery(pageBean);
		
		String[] exclude = {"pageNum","pageSize","dc","subareas","decidedzone"};
		this.writeObject2Json(pageBean, exclude);
		return NONE;
	}
	
	/**
	 * 查询所有未关联分区
	 * @return
	 */
	public String listajax(){
		//查询所有未关联的分区
		List<Subarea> list = subareaService.findNoLinked();
		
		String[] excludes = {"region","decidedzone"};
		this.writeList2Json(list, excludes);
		return NONE;
	}
	
	/**
	 * 修改分区
	 */
	public String modify(){
		subareaService.edit(model);
		return "list";
	}
	
	
}
