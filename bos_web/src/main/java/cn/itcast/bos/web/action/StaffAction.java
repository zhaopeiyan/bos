package cn.itcast.bos.web.action;

import java.io.IOException;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.service.IStaffService;
import cn.itcast.bos.utils.PageBean;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@Scope("prototype")
public class StaffAction extends BaseAction<Staff> {

	@Autowired
	private IStaffService staffService;

	@RequiresPermissions("staff")
	public String add() {
		staffService.add(model);
		return "add";
	}

	/**
	 * 分页查询staff信息
	 * 
	 * @return
	 * @throws IOException
	 */
	public String pageQuery() throws IOException {

		DetachedCriteria dc = pageBean.getDc();

		String name = model.getName();
		if (name != null) {
			dc.add(Restrictions.like("name", "%" + name + "%"));
		}

		String telephone = model.getTelephone();
		if (null != telephone) {
			dc.add(Restrictions.like("telephone", "%" + telephone + "%"));
		}

		String standard = model.getStandard();
		if (null != standard) {
			dc.add(Restrictions.like("standard", "%" + standard + "%"));
		}

		staffService.pageQuery(pageBean);

		String[] exclude = { "pageNum", "pageSize", "dc", "decidedzones", "workbills", "noticebills" };
		writeObject2Json(pageBean, exclude);
		return NONE;
	}

	private String ids;

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String delStaff() {

		staffService.delStaff(ids);
		return "del";
	}

	public String reStaff() {
		// 编程控制方式
		// 1.获取subject
		Subject subject = SecurityUtils.getSubject();
		// 2.使用subject校验权限
		subject.checkPermission("abc");
		staffService.reStaff(ids);
		return "restore";
	}

	public String edit() {
		staffService.edit(model);
		return "edit";
	}

	public String listajax() {
		// 1.查询未作废的(deltag='0'的所有的取派员)
		List<Staff> list = staffService.findNoDel();
		String[] excludes = { "decidedzones", "workbills", "noticebills" };
		this.writeList2Json(list, excludes);
		return NONE;
	}

}
