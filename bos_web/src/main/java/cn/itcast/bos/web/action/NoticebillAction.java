package cn.itcast.bos.web.action;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Noticebill;
import cn.itcast.bos.service.INoticebillService;
import cn.itcast.crm.service.Customer;
import cn.itcast.crm.service.ICustomerService;

@Controller
@Scope("prototype")
public class NoticebillAction extends BaseAction<Noticebill> {

	@Resource
	private INoticebillService noticebillService;
	@Resource
	private ICustomerService customerService;
	
	/**
	 * 更新
	 */

	public String update(){
		String id = model.getId();
		String staffId = model.getStaff().getId();
		noticebillService.update(staffId,id);
		return "list";
	}
	
	/**
	 * 添加noticebill信息
	 * @return
	 */
	public String add(){
		noticebillService.add(model);
		return "list";
	}
	
	/**
	 * 异步查询客户信息
	 * @return
	 */
	public String findCustoemrByTelephone(){
		Customer custoemr = customerService.findCustoemrByTelephone(model.getTelephone());
		String[] exclude = {};
		this.writeObject2Json(custoemr, exclude );
		return NONE;
	}
	
	
	/**
	 * 分页查询
	 * @return
	 */
	public String pageQuery(){
		
		noticebillService.pageQuery(pageBean);
		
		String[] exclude = {"user","decidedzones","workbills","noticebills"};
		writeObject2Json(pageBean, exclude);
		return NONE;
	}
	
	private String pickDate;
	public String getPickDate() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
		String format = simpleDateFormat.format(pickDate);
		
		System.out.println(format);
		return format;
	}

	
	/**
	 * 查询所有工单信息
	 */
	public String findnoassociations(){
		return NONE; 
	}
	
}
