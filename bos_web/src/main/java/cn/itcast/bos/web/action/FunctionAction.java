package cn.itcast.bos.web.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Function;
import cn.itcast.bos.domain.User;
import cn.itcast.bos.service.IFunctionService;
import cn.itcast.bos.utils.BosUtils;

@Controller
@Scope("prototype")
public class FunctionAction extends BaseAction<Function> {

	@Resource
	private IFunctionService functionService;
	
	/**
	 * 根据用户查询当前的菜单
	 * @return
	 */
	public String findMenu(){
		//1.获取当前登录用户
		User user = BosUtils.getUser();
		List<Function> list = null;
		if("admin".equals(user.getUsername())){
			//2.如果是admin,返回所有菜单(generatemenu='1' and user.i=?)
			list = functionService.findAllMenu(user.getId());
		}else{
			//3.如果是普通用户,根据用户id查询菜单
			list = functionService.findMenuByUserId(user.getId());
		}
		
		String[] exclude = {"function","functions","roles"};
		this.writeList2Json(list, exclude );
		return NONE;
	}
	
	
	/**
	 * 异步查询所有父功能点
	 * @return
	 */
	public String listajax(){
		List<Function> list = functionService.findAll();
		String[] exclude = {"function","functions","roles"};
		writeList2Json(list, exclude);
		return NONE;
	}
	
	/**
	 * 添加function
	 * @return
	 */
	public String add(){
		functionService.add(model);
		return "list";
	}
	
	/**
	 * 分页查询
	 */
	public String pageQuery(){
		//从model中获取page
		String page = model.getPage();
		//将page封装回pageBean
		pageBean.setPageNum(Integer.parseInt(page));
		//2.执行分页查询
		functionService.pageQuery(pageBean);
		String[] exclude = {"pageNum","pageSize","function","functions","roles"};
		writeObject2Json(pageBean, exclude );
		return NONE;
	}
	
}
