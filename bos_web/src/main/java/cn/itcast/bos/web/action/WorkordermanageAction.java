package cn.itcast.bos.web.action;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.Workordermanage;
import cn.itcast.bos.service.IWorkordermanageService;

@Controller
@Scope("prototype")
public class WorkordermanageAction extends BaseAction<Workordermanage> {

	@Resource
	private IWorkordermanageService workordermanageService;
	
	public String pageQuery(){
		workordermanageService.pageQuery(pageBean);
		String[] exclude = {};
		writeObject2Json(pageBean, exclude );
		return NONE;
	}
	
	public String add(){
		String flag = "1";
		try {
			workordermanageService.add(model);
		} catch (Exception e) {
			flag = "0";
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		try {
			ServletActionContext.getResponse().getWriter().print(flag);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		return NONE;
	}
	
	
	
	
	private File workbillFile;
	public void setWorkbillFile(File workbillFile) {
		this.workbillFile = workbillFile;
	}

	/**
	 * 批量上传
	 */
	public String batchImport(){
		//使用POI读取Excel文档
		String flag = "1";
		try {
			//创建Wordbook对象,读取整个文档
			HSSFWorkbook wb = new HSSFWorkbook(new FileInputStream(workbillFile));
			//读取某个sheet
			HSSFSheet sheet = wb.cloneSheet(0);
			//循环读取某一行
			List<Workordermanage> list = new ArrayList<Workordermanage>();
			for(Row row : sheet){
				//跳过第一行
				int rowNum = row.getRowNum();
				if(0 == rowNum){
					//第一行,跳过
					continue;
				}
				//读取一行中的单元格
				//String id = row.getCell(0).getStringCellValue()
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			throw new RuntimeException("" + e);
		}
		return NONE;
	}
	
}
