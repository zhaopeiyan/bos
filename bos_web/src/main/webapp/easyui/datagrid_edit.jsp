<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>datagrid_edit</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>

</head>
<body>

	<table id="grid"></table>
	<script type="text/javascript">
		$(function(){
			$('#grid').datagrid({
				url:'${pageContext.request.contextPath}/json/staff.json',
				columns:[[
				          {title:'编号',field:'id',checkbox:true},
				          {title:'姓名',field:'name',editor:{
				        	  type:'validatebox',
				        	  options:{
				        		  required:true
				        	  }
				          }},
				          {title:'电话',field:'telephone',editor:{
				        	  type:'validatebox',
				        	  options:{
				        		  required:true
				        	  }
				          }}
				          ]],
				toolbar:[
				         {text:'增加',iconCls:'icon-add',handler:function(){
				        	 //点击时要在datagrid上添加一行'
				        	 $('#grid').datagrid('insertRow',{
				        		 //在第一行的位置插入一个空行
				        		 index:0,
				        		 //添加一个空行
				        		 row:{}
				        	 });
				        	 //开启第一行的编辑状态
				        	 $('#grid').datagrid('beginEdit',0);
				         }},
				         {text:'删除',handler:function(){
				        	 //1.获取要删除的数据
				        	 var rows = $('#grid').datagrid('getSelections');
				        	 //2.未选中,提示
				        	 if(rows.length <= 0){
				        		 $.messager.alert("提示","请选择要删除的行!",'warning');
				        	 }else{
					        	 //3.选中,确认是否删除
				        		 $.messager.confirm("确认",'确定删除?',function(r){
				        			 if(r){
				        				 for(var i = 0 ; i <rows.length ; i++){
				        					 var index = $('#grid').datagrid('getRowIndex',rows[i]);
				        					 alert(index);
				        					 $('#grid').datagrid('deleteRow',index);
				        				 }
				        			 }
				        		 });
					        	 
				        	 }
				         }},
				         {text:'修改',handler:function(){
				        	 //1.获取要删除的数据
				        	 var rows = $('#grid').datagrid('getSelections');
				        	 //2.未选中,提示
				        	 if(rows.length <= 0 || rows.length > 1){
				        		 $.messager.alert("提示","请选择要删除的行!",'warning');
				        	 }else{
					        	 //3.选中,确认是否删除
				        		 $.messager.confirm("确认",'确定修改?',function(r){
				        			 if(r){
				        				 //开启编辑状态
			        					 var index = $('#grid').datagrid('getRowIndex',rows[0]);
				        				 $('#grid').datagrid('beginEdit',index);
				        			 }
				        		 });
				        	 }
				         }},
				         {text:'保存',handler:function(){
				        	 //1.获取要保存的数据
				        	 var rows = $('#grid').datagrid('getSelections');
				        	 //2.未选中,提示
				        	 if(rows.length <= 0 || rows.length > 1){
				        		 $.messager.alert("提示","请选择要保存的行!",'warning');
				        	 }else{
					        	 //3.选中,确认是否删除
				        		 $.messager.confirm("确认",'确定修改?',function(r){
				        			 if(r){
				        				 //关闭编辑状态
			        					 var index = $('#grid').datagrid('getRowIndex',rows[0]);
				        				 $('#grid').datagrid('endEdit',index);
				        			 }
				        		 });
				        	 }
				         }},
				         {text:'查询'}
				         ],
				 pagination:true,
				 pageList:[10,20,30],
				 onAfterEdit:function(rowIndex,rowData,changes){
					 alert(rowIndex+"-"+rowData.name+"-"+changes.name);
					 
				 }
			})
		})
	</script>
</body>
</html>