<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>easyui-layout</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>

</head>
<body class="easyui-layout" fit="true">

	<!-- 北部区域 -->
	<div title="XXX管理系统" data-options="region:'north'" style="height:100px"></div>
	
	<!-- 西部区域 -->
	<div title="功能菜单" data-options="region:'west'" style="width:200px">
		<div class="easyui-accordion" fit="true">
			<div title="Title1" data-options="iconCls:'icon-save'" style="overflow:auto;padding:10px;">   
		       <!-- 基于标准json构建ztree树 -->
		    	<!-- 1.提供一个容器 -->
		    	<ul id="ztree1" class="ztree"></ul>
		    	<script type="text/javascript">
		    		$(function(){
		    			//2.定义全局的setting变量
		    			var setting = {};//标准json的setting可以为空,可以使用默认值
		    			//3.定义树节点变量
		    			var nodes = [
		    			             {name:'系统管理'},
		    			             {name:'用户管理',children:[
		    			                                    {name:'用户添加'},
		    			                                    {name:'用户修改'},
		    			                                    {name:'用户删除'}
		    			                                    ]},
		    			             {name:'权限管理'}
		    			             ];
		    			//4.初始化树
		    			$.fn.zTree.init($('#ztree1'),setting,nodes);
		    		})
		    	</script> 
		    </div>   
		    <div title="Title2" data-options="iconCls:'icon-reload'" style="padding:10px;">   
		        
		        <!-- 基于简单json构建ztree -->
		        <ul id="ztree2" class="ztree"></ul>
		        <script type="text/javascript">
		        	$(function(){
		        		//2.定義全局的setting變量
		        		var setting = {
		        				data:{
		        					simpleData:{
		        						enable:true//開啓簡單json數據功能
		        					}
		        				}
		        		};//標準json的setting可以爲空，可以使用默認值
		        		//3.定義樹節點變量
		        		var nodes = [
		        		             {id:'1',pId:'0',name:'系统管理'},
		        		             {id:'2',pId:'0',name:'用户管理'},
		        		             {id:'21',pId:'2',name:'用户添加'},
		        		             {id:'22',pId:'2',name:'用户修改'},
		        		             {id:'23',pId:'2',name:'用户删除'},
		        		             {id:'3',pId:'0',name:'权限管理'}
		        		             ];
		        		//4.初始化树
		        		$.fn.zTree.init($('#ztree2'),setting,nodes);
		        	});
		        </script>
		        
		    </div>   
		    <div title="Title3" data-options="iconCls:'icon-reload',selected:true">   
		        <ul id="ztree3" class="ztree"></ul>
		        <script type="text/javascript">
		        	$(function(){
		        		//2.定義全局的setting變量
		        		var setting = {
		        				data:{
		        					simpleData:{
		        						enable:true//開啓簡單json數據功能
		        					}
		        				},
		        				callback:{
		        					onClick:function(event,treeId,treeNode){
		        						//添加动态添加选项卡的操作
		        						//判断是否是根节点,非根节点才能添加选项卡
		        						var page = treeNode.page;
		        						if(undefined != page){
		        							//有page属性是子节点
		        							//重复的选项卡只能打开一次
		        							//判断当前选项卡中是否存在当前添加的选项卡
			        						//alert(page);
			        						var b = $('#tt').tabs('exists',treeNode.name);
			        						//alert(b);
			        						if(b){
			        							//已存在选项卡:选中
			        							$("#tt").tabs("select",treeNode.name);
			        						}else{
			        							//不存在:添加
			        							$('#tt').tabs('add',{
			        								title:treeNode.name,
			        								content:'<iframe src="${pageContext.request.contextPath}/'+page+'" style="height:100%;width:100%" frameborder="0"></iframe>',
			        								closable:true
			        							});
			        						}
		        						}
		        					}
		        				}
		        		};//標準json的setting可以爲空，可以使用默認值
		        		//3.基於ajax加載節點數據
		        		var url = "${pageContext.request.contextPath}/json/menu.json";
		        		$.post(url,{},function(data){
		        			//4.初始化樹
		        			$.fn.zTree.init($('#ztree3'),setting,data);
		        		},'json')
		        	});
		        </script>
		            
		    </div>   
		   
		</div>
	</div>
	
	<!-- 中部区域 -->
	<div data-options="region:'center'">
		<div id="tt" class="easyui-tabs" fit="true">   
		    <div title="Tab1" style="padding:20px;display:none;">   
		        tab1    
		    </div>   
		    <div title="Tab2" data-options="closable:true" style="overflow:auto;padding:20px;display:none;">   
		        tab2    
		    </div>   
		    <div title="Tab3" data-options="iconCls:'icon-reload',closable:true" style="padding:20px;display:none;">   
		        tab3    
		    </div>   
		</div>  
	</div>
	
	<div data-options="region:'east'" style="width:100px">东部区域</div>
	<div data-options="region:'south'"  style="height:100px">南部区域</div>
</body>
</html>