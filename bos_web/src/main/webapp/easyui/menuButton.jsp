<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>easyui-menuButton</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>

</head>
<body>
	<a href='javascript:;' id='mb' class='easyui-menubutton' data-options="menu:'#mm',iconCls:'icon-edit'">edit</a>
	<div id="mm" style="width:150px;">
		<div data-options="iconCls:'icon-undo'">Undo</div>   
	    <div data-options="iconCls:'icon-redo'">Redo</div>   
	    <div class="menu-sep"></div>   
	    <div>Cut</div>   
	    <div>Copy</div>   
	    <div>Paste</div>   
	    <div class="menu-sep"></div>   
	    <div data-options="iconCls:'icon-remove'">Delete</div>   
	    <div>Select All</div>   
	</div>
	<hr>
	<a href="javascript:;" id="mb2" class="easyui-menubutton" data-options="menu:'#mm2',iconCls:'icon-help'">XXX</a>
	<div id="mm2" style="width:150px">
		<div onClick="doAlert();">修改密码</div>
		<script type="text/javascript">
			function doAlert(){
				alert(1);
			}
		</script>
		<div>联系管理员</div>
		<div class='menu-sep'></div>
		<div>退出系统</div>
	</div>
	
</body>
</html>





















