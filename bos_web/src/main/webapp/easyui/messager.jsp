<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>easyui-messager</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>

</head>
<body>
	<a class="easyui-linkbutton" onClick="$.messager.alert('提示框','提示框内容','info');">info</a>
	<a class="easyui-linkbutton" onClick="$.messager.alert('提示框','提示框内容','error');">error</a>
	<a class="easyui-linkbutton" onClick="$.messager.alert('提示框','提示框内容','question');">question</a>
	<a class="easyui-linkbutton" onClick="$.messager.alert('提示框','提示框内容','warning');">warning</a>
	<br>
	<a class="easyui-linkbutton" onClick=confirmDel()>confirm</a>
	
	<script type="text/javascript">
		function confirmDel(){
			$.messager.confirm('确认','确定进行操作吗?',function(r){
				if(r){
					//alert('删除成功!')
					$.messager.alert('configuration','删除成功');
				}
			});
		}
		//欢迎框
		window.setTimeout(function (){
			$.messager.show({
				title:'welcome',
				msg:'欢迎登陆XXX系统',
				timeout:2000,
				showType:'slide'
			});
		},2000);//设置页面加载完成后延迟2秒加载
	</script>
</body>
</html>







