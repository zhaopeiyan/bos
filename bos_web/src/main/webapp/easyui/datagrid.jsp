<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>easyui-datagrid</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/ztree/zTreeStyle.css" type="text/css">

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/ztree/jquery.ztree.all-3.5.js"></script>


</head>
<body>
	<h2>通过ajax动态创建datagrid控件</h2>
	<table id="dg"></table>
	<script type="text/javascript">
		$('#dg').datagrid({
			url : '${pageContext.request.contextPath}/json/staff.json',
			columns:[[
			          {field:'id',title:'id',width:100},
			          {field:'name',title:'name',width:100},
			          {field:'telephone',title:'telephone',width:100},
			          {field:'haspda',title:'haspda',width:100},
			          {field:'deltag',title:'deltag',width:100},
			          {field:'standard',title:'standard',width:100},
			          {field:'station',title:'station',width:100}
			          ]],
			 toolbar:[
			          {text:'查询',iconCls:'icon-search',handler:function(){
			        	  alert(1);
			          }},
			          {text:'增加'},
			          {text:'作废'},
			          {text:'还原'}
			          ],
			 singleSelect:true,//开启表格单选功能
			 pagination:true,//开启datagrid的分页条
			 pageList:[1,2,3]//自定义每页显示条数
		});
	</script>
	<hr>

</body>
</html>