<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- 导入jquery核心类库 -->
<script type="text/javascript"
	src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<!-- 导入easyui类库 -->
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/js/easyui/themes/icon.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/js/easyui/ext/portal.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath }/css/default.css">	
<script type="text/javascript"
	src="${pageContext.request.contextPath }/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/js/easyui/ext/jquery.portal.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath }/js/easyui/ext/jquery.cookie.js"></script>
<script
	src="${pageContext.request.contextPath }/js/easyui/locale/easyui-lang-zh_CN.js"
	type="text/javascript"></script>
<script type="text/javascript">
	function doAdd(){
		//alert("增加...");
		$('#addStaffWindow').window("open");
	}
	
	function doView(){
		//alert("查看...");
		$('#searchWindow').window("open");
	}
	
	function doDelete(){
		//获取选中条数
		var rows = $("#grid").datagrid("getSelections");
		if(rows.length <= 0){
			//如果未选中，弹出提示
			$.messager.alert('提示信息','请您选择要删除的数据！','warning');
		}else{
			$.messager.confirm('确认信息','确定要删除吗?',function(r){
				if(r){
					var staffArr = new Array();
					//alert(rows.length);
					for(var i = 0 ; i < rows.length ; i++ ){
						staffArr.push(rows[i].id);
					}
					var ids = staffArr.join(",");
					location.href = '${pageContext.request.contextPath}/staffAction_delStaff.action?ids='+ids;
				}
			});
		}
	}
	
	function doRestore(){
		//alert("将取派员还原...");
		//获取选中条数
		var rows = $("#grid").datagrid("getSelections");
		//alert(rows);
		if(rows.length <= 0){
			$.messager.alert('提示信息','您未选任何员工','warning');
		}else{
			$.messager.confirm('提示信息','确定要恢复选中项吗?',function (r){
				if(r){
					var staffArr = new Array();
					for(var i = 0 ; i < rows.length ; i++){
						staffArr.push(rows[i].id);
					}
					var ids = staffArr.join(",");
					location.href = "${pageContext.request.contextPath}/staffAction_reStaff.action?ids="+ids;
				}
			});
		}
	}
	//工具栏
	var toolbar = [ {
		id : 'button-view',	
		text : '查询',
		iconCls : 'icon-search',
		handler : doView
	}, {
		id : 'button-add',
		text : '增加',
		iconCls : 'icon-add',
		handler : doAdd
	}, 
	<shiro:hasPermission name="staff">
		{
			id : 'button-delete',
			text : '作废',
			iconCls : 'icon-cancel',
			handler : doDelete
		},
	</shiro:hasPermission>
	{
		id : 'button-save',
		text : '还原',
		iconCls : 'icon-save',
		handler : doRestore
	}
	];
	// 定义列
	var columns = [ [ {
		field : 'id',
		checkbox : true,
	},{
		field : 'name',
		title : '姓名',
		width : 120,
		align : 'center'
	}, {
		field : 'telephone',
		title : '手机号',
		width : 120,
		align : 'center'
	}, {
		field : 'haspda',
		title : '是否有PDA',
		width : 120,
		align : 'center',
		formatter : function(data,row, index){
			if(data=="1"){
				return "有";
			}else{
				return "无";
			}
		}
	}, {
		field : 'deltag',
		title : '是否作废',
		width : 120,
		align : 'center',
		formatter : function(data,row, index){
			if(data=="0"){
				return "正常使用"
			}else{
				return "已作废";
			}
		}
	}, 
	{
		field : 'email',
		title : '邮箱',
		width : 120,
		align : 'center'
	},{
		field : 'standard',
		title : '取派标准',
		width : 120,
		align : 'center'
	}, {
		field : 'station',
		title : '所谓单位',
		width : 200,
		align : 'center'
	} ] ];
	
	$(function(){
		// 先将body隐藏，再显示，不会出现页面刷新效果
		$("body").css({visibility:"visible"});
		
		// 查询分区
		$('#searchWindow').window({
	        title: '查询取派员',
	        width: 400,
	        modal: true,
	        shadow: true,
	        closed: true,
	        height: 400,
	        resizable:false
	    });
		$("#btn").click(function(){
			//alert("执行查询...");
			//1.获取到所有的查询条件
			var data = $('#searchForm').serializeJson();
			//2.发送分页查询请求,查询数据,显示到datagrid中
			$('#grid').datagrid('load',data);
			//3.查询后关闭searchForm
			$("#searchWindow").window('close');
		});
		
		
		
		// 取派员信息表格
		$('#grid').datagrid( {
			iconCls : 'icon-forward',
			fit : true,
			border : true,
			rownumbers : true,
			striped : true,
			pageNumbe:1,
			pageSize:5,
			pageList: [15,30,45],
			pagination : true,
			toolbar : toolbar,
			url : "${pageContext.request.contextPath}/staffAction_pageQuery.action",
			idField : 'id',
			columns : columns,
			onDblClickRow : doDblClickRow
		});
		
		// 添加取派员窗口
		$('#addStaffWindow').window({
	        title: '添加取派员',
	        width: 400,
	        modal: true,
	        shadow: true,
	        closed: true,
	        height: 400,
	        resizable:false
	    });
		
		// 修改取派员窗口
		$('#editStaffWindow').window({
	        title: '修改取派员',
	        width: 400,
	        modal: true,
	        shadow: true,
	        closed: true,//默认关闭此窗口
	        height: 400,
	        resizable:false
	    });
		
		//给表单绑定序列化函数
		$.fn.serializeJson=function(){  
            var serializeObj={};  
            var array=this.serializeArray();
            $(array).each(function(){  
                if(serializeObj[this.name]){  
                    if($.isArray(serializeObj[this.name])){  
                        serializeObj[this.name].push(this.value);  
                    }else{  
                        serializeObj[this.name]=[serializeObj[this.name],this.value];  
                    }  
                }else{  
                    serializeObj[this.name]=this.value;   
                }  
            });  
            return serializeObj;  
        };
		
	});

	function doDblClickRow(rowIndex, rowData){
		//1.打开编辑窗口
		$('#editStaffWindow').window('open');
		//2.在窗口中回显数据
		$('#editStaffForm').form('load',rowData);
	}
</script>	
</head>
<body class="easyui-layout" style="visibility:hidden;">
	<div region="center" border="false">
    	<table id="grid"></table>
	</div>
	
	<!-- staff查询 -->
	
	<div class="easyui-window" title="查询staff窗口" id="searchWindow" collapsible="false" minimizable="false" maximizable="false" style="top:20px;left:200px">
		<div style="overflow:auto;padding:5px;" border="false">
			<form id="searchForm" action="${pageContext.request.contextPath}/staffAction_pageQuery.action" method="post">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">收派员信息</td>
					</tr>
					<tr>
						<td>姓名</td>
						<td><input type="text" name="name" class="easyui-validatebox" /></td>
					</tr>
					<tr>
						<td>手机</td>
						<td>
							<input type="text" name="telephone" class="easyui-validatebox" />
						</td>
					</tr>
					<tr>
						<td>取派标准</td>
						<td>
							<input type="text" name="standard" class="easyui-validatebox" />  
						</td>
					</tr>
					<tr>
						<td colspan="2"><a id="btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">查询</a> </td>
					</tr>
					</table>
			</form>
		</div>
	</div>
	
	
	<!-- staff添加 -->
	<div class="easyui-window" title="对收派员进行添加或者修改" id="addStaffWindow" collapsible="false" minimizable="false" maximizable="false" style="top:20px;left:200px">
		<div region="north" style="height:31px;overflow:hidden;" split="false" border="false" >
			<div class="datagrid-toolbar">
				<a id="save" icon="icon-save" href="#" class="easyui-linkbutton" plain="true" >保存</a>
				<script type="text/javascript">
					$(function(){
						$('#save').click(function(){
							var v = $('#addStaffForm').form('validate');
							if(v){
								//验证成功,提交表单
								$('#addStaffForm').submit();
							}
						})
					})
					
				</script>
			</div>
		</div>
		
		<div region="center" style="overflow:auto;padding:5px;" border="false">
			<form id="addStaffForm" method="post" action="${pageContext.request.contextPath }/staffAction_add.action">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">收派员信息</td>
					</tr>
					<!-- TODO 这里完善收派员添加 table -->
					<!-- <tr>
						<td>取派员编号</td>
						<td><input type="text" name="id" class="easyui-validatebox" required="true"/></td>
					</tr> -->
					<tr>
						<td>姓名</td>
						<td><input type="text" name="name" class="easyui-validatebox" required="true"/></td>
					</tr>
					<tr>
						<td>手机</td>
						<td>
							<input type="text" name="telephone" class="easyui-validatebox" data-options="validType:'phone'" required="true"/>
							<script type="text/javascript">
								$(function(){
									$.extend($.fn.validatebox.defaults.rules,{
										phone:{
											validator:function(value){
												//验证手机号的正则表达式,在js中定义正则表达式需要写在//中间
												var reg = /^1[3,5,7,8][0-9]{9}$/;
												return reg.test(value);
											},
											message: '请输入正确的手机号码!'
										}
									})
								})
							</script>
						</td>
					</tr>
					<tr>
						<td>单位</td>
						<td><input type="text" name="station" class="easyui-validatebox" required="true"/></td>
					</tr>
					<tr>
						<td>邮箱</td>
						<td><input type="text" name="email" class="easyui-validatebox" data-options="validType:'email'" required="true"/></td>
					</tr>
					<tr>
						<td colspan="2">
						<input type="checkbox" name="haspda" value="1" />
						是否有PDA</td>
					</tr>
					<tr>
						<td>取派标准</td>
						<td>
							<input type="text" name="standard" class="easyui-validatebox" required="true"/>  
						</td>
					</tr>
					</table>
			</form>
		</div>
	</div>
	
	<!-- staff修改 -->
	<div class="easyui-window" title="对收派员进行添加或者修改" id="editStaffWindow" collapsible="false" minimizable="false" maximizable="false" style="top:20px;left:200px">
		<div region="north" style="height:31px;overflow:hidden;" split="false" border="false" >
			<div class="datagrid-toolbar">
				<a id="edit" icon="icon-save" href="#" class="easyui-linkbutton" plain="true" >保存</a>
				<script type="text/javascript">
					$(function(){
						$('#edit').click(function(){
							var v = $('#editStaffForm').form('validate');
							if(v){
								//验证成功,提交表单
								$('#editStaffForm').submit();
							}
						})
					})
					
				</script>
			</div>
		</div>
		
		<div region="center" style="overflow:auto;pediting:5px;" border="false">
			<form id="editStaffForm" method="post" action="${pageContext.request.contextPath }/staffAction_edit.action">
				<table class="table-edit" width="80%" align="center">
					<tr class="title">
						<td colspan="2">收派员信息</td>
					</tr>
					<!-- TODO 这里完善收派员添加 table -->
					<tr>
						<td>取派员编号</td>
						<td><input type="hidden" name="id" class="easyui-validatebox" required="true"/></td>
					</tr>
					<tr>
						<td>姓名</td>
						<td><input type="text" name="name" class="easyui-validatebox" required="true"/></td>
					</tr>
					<tr>
						<td>手机</td>
						<td>
							<input type="text" name="telephone" class="easyui-validatebox" data-options="validType:'phone'" required="true"/>
							<script type="text/javascript">
								$(function(){
									$.extend($.fn.validatebox.defaults.rules,{
										phone:{
											validator:function(value){
												//验证手机号的正则表达式,在js中定义正则表达式需要写在//中间
												var reg = /^1[3,5,7,8][0-9]{9}$/;
												return reg.test(value);
											},
											message: '请输入正确的手机号码!'
										}
									})
								})
							</script>
						</td>
					</tr>
					<tr>
						<td>单位</td>
						<td><input type="text" name="station" class="easyui-validatebox" required="true"/></td>
					</tr>
					<tr>
						<td>邮箱</td>
						<td><input type="text" name="email" class="easyui-validatebox" data-options="validType:'email'" required="true"/></td>
					</tr>
					<tr>
						<td colspan="2">
						<input type="checkbox" name="haspda" value="1" />
						是否有PDA</td>
					</tr>
					<tr>
						<td>取派标准</td>
						<td>
							<input type="text" name="standard" class="easyui-validatebox" required="true"/>  
						</td>
					</tr>
					</table>
			</form>
		</div>
	</div>
	
	
</body>
</html>	