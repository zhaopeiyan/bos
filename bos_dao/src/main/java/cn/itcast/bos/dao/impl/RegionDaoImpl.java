package cn.itcast.bos.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.itcast.bos.dao.IRegionDao;
import cn.itcast.bos.domain.Region;

@Repository
@SuppressWarnings("unchecked")
public class RegionDaoImpl extends BaseDaoImpl<Region>implements IRegionDao {

	@Override
	public List<Region> findByQ(String q) {
		
		String hql = "from Region where shortcode like ? or citycode like ?";
		List<Region> list = (List<Region>) this.getHibernateTemplate().find(hql, "%"+q+"%", "%"+q+"%");
		return list;
	}

	

}
