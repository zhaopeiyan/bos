package cn.itcast.bos.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import cn.itcast.bos.dao.IBaseDao;
import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.utils.PageBean;


public class BaseDaoImpl<T> extends HibernateDaoSupport implements IBaseDao<T> {

	private Class<T> entityClass;
	
	public BaseDaoImpl(){
		ParameterizedType parameterizedType = (ParameterizedType) this.getClass().getGenericSuperclass();
		Type[] types = parameterizedType.getActualTypeArguments();
		entityClass = (Class<T>) types[0];
	}
	
	@Resource//1.注入到属性上,从容器中获取实例赋给属性  2.注入到set方法上,从容器中获取实例赋值给参数
	public final void setMySessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	
	@Override
	public void saveOrUpdate(T entity) {
		this.getHibernateTemplate().saveOrUpdate(entity);
	}
	
	@Override
	public void save(T entity) {
		this.getHibernateTemplate().save(entity);
	}

	@Override
	public void update(T entity) {
		this.getHibernateTemplate().update(entity);
	}

	@Override
	public void delete(T entity) {
		this.getHibernateTemplate().delete(entity);
	}

	@Override
	public T findById(Serializable id) {
		return this.getHibernateTemplate().get(entityClass, id);
	}

	@Override
	public List<T> findAll() {
		String hql = "from " + entityClass.getName();
		return (List<T>) this.getHibernateTemplate().find(hql);
	}

	
	@Override
	public void executeUpdate(String namedQuery, Object... objects) {
		//1.获取当前的session对象
		Session session = this.getSessionFactory().getCurrentSession();
		
		Query query = session.getNamedQuery(namedQuery);
		
		int index = 0;
		for(Object obj : objects){
			query.setParameter(index++, obj);
		}
		query.executeUpdate();
		
	}

	/**
	 * 分页查询通用方法
	 */
	@Override
	public void pageQuery(PageBean pb) {
		DetachedCriteria dc = pb.getDc();
		//查询记录总数
		//设置投影查询查询
		dc.setProjection(Projections.rowCount());
		List<Long> rowList = (List<Long>) this.getHibernateTemplate().findByCriteria(dc);
		pb.setTotal(rowList.get(0));
		//恢复正常查询
		dc.setProjection(null);
		
		//绑定返回的数据到指定的对象上,ROOT_ENTITY相当于创建查询条件时绑定的实体类对象
		dc.setResultTransformer(DetachedCriteria.ROOT_ENTITY);
		//每页显示几条记录
		int maxResults = pb.getPageSize();
		//设置开始位置索引
		int firstResult = ((pb.getPageNum() - 1) * pb.getPageSize());
		List<T> rows = (List<T>) this.getHibernateTemplate().findByCriteria(dc, firstResult , maxResults);
		pb.setRows(rows);  
		
	}

	@Override
	public List<T> findByConditions(String namedQuery, Object... objects) {
		//1.获取当前的session对象
		Session currentSession = this.getSessionFactory().getCurrentSession();
		//2.使用当前的session对象根据命名查询的名称获取HQL
		Query query = currentSession.getNamedQuery(namedQuery);
		//3.通过query的接口给HQL中的?赋值
		int index = 0;
		for(Object obj : objects){
			//参数1:问号的索引从0开始
			//参数2:要赋值的obj
			query.setParameter(index++, obj);
		}
		return query.list();
	}
	
}
