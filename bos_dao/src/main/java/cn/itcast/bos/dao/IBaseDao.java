package cn.itcast.bos.dao;

import java.io.Serializable;
import java.util.List;

import cn.itcast.bos.domain.Staff;
import cn.itcast.bos.utils.PageBean;

/**
 * <p>Title:IBaseDao</p>
 * <p>Description:</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年4月11日 下午7:03:12
 * @param <T>
 */
public interface IBaseDao<T> {

	public void save(T entity);
	public void update(T entity);
	public void delete(T entity);
	
	public T findById(Serializable id);
	public List<T> findAll();
	void executeUpdate(String namedQuery, Object... objects);
	List<T> findByConditions(String namedQuery, Object... objects);
	void pageQuery(PageBean pb);
	void saveOrUpdate(T entity);
}
