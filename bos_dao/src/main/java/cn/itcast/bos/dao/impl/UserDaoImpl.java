package cn.itcast.bos.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import cn.itcast.bos.dao.IUserDao;
import cn.itcast.bos.domain.User;

/**
 * <p>Title:UserDaoImpl</p>
 * <p>Description:</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年4月11日 下午7:02:48
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao {

	@Override
	public User login(String username, String md5) {
		String hql = "from User where username=? and password=?";
		List<User> list = (List<User>) this.getHibernateTemplate().find(hql,username, md5);
		if(list.size()>0 && list != null){
			return list.get(0);
		}
		return null;
	}

	@Override
	public User findByUsername(String username) {
		String hql = "from User where username = ?";
		List<User> list = (List<User>) this.getHibernateTemplate().find(hql, username);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

}
