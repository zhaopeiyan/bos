package cn.itcast.bos.dao;

import cn.itcast.bos.domain.User;

/**
 * <p>Title:IUserDao</p>
 * <p>Description:</p>
 * <p>Company:</p>
 * @author PeiyanZhao
 * @date2017年4月11日 下午7:03:07
 */
public interface IUserDao extends IBaseDao<User> {

	User login(String username, String md5);

	User findByUsername(String username);

	
	
}
